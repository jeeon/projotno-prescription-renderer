var template = require('./prescription.handlebars');
var ProjotnoPrescriptionRenderer = require('./projotno-prescription-renderer.js');
var $ = require('jquery');

$(document).ready(function () {
  $('#manual-ratio').on('change', function () {
    $('#ratio').attr('disabled', !$('#manual-ratio').is(':checked'));
  });

  $('#button-image-window').click(function () {
    go('image/window');
  });
  $('#button-image-data').click(function () {
    go('image/data');
  });
  $('#button-image-datauri').click(function () {
    go('image/datauri');
  });

  $('#button-pdf-window').click(function () {
    go('pdf/window');
  });
  $('#button-pdf-data').click(function () {
    go('pdf/data');
  });
  $('#button-pdf-datauri').click(function () {
    go('pdf/datauri');
  });
  $('#button-pdf-print').click(function () {
    go('pdf/print');
  });
});
function go(outputMode) {
  $.ajax("prescription.sample/prescription.json").done(function (prescription) {
    ProjotnoPrescriptionRenderer.render(
      template,
      prescription,
      $('input[name=orientation]:checked').val(),
      outputMode,
      $('#manual-ratio').is(':checked') ? $('#ratio').val() : undefined,
      function (res) {
        if (outputMode.split('/')[1].startsWith('data')) console.log(res);
      },
      function (err) {
        console.error(err);
      });
  });
}
